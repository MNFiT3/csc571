package com.mnfit3dev.labassignment1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mnfit3dev.labassignment1.model.User;

public class LoginActivity extends AppCompatActivity {
    TextView email, password;
    DBHelper db;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = new DBHelper(getApplicationContext());
        session = new Session(getApplicationContext());

        if(session.loggedIn()){
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            intent.putExtra("userEmail", session.userEmail());
            startActivity(intent);
            finish();
            return;
        }

        //Create an account if there is no account
        if(db.userCount() == 0){
            db.userRegister(new User(null, "admin", "admin@mail.com", "1234567890"));
            db.userRegister(new User(null, "user", "user@mail.com", "1234567890"));
        }
        Log.w("DB", db.userCount() + "");
    }

    public void toastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void Login(View view){
        boolean error = false;
        String message = "";

        email = findViewById(R.id.txtEmailRegister);
        password = findViewById(R.id.txtRePasswordRegister);

        if(email.length() == 0){
            toastMessage("Email cannot be empty");
            error = true;
        }

        if(password.length() == 0 && message.length() == 0){
            toastMessage("Minimum length for password is 8");
            error = true;
        }

        User user;
        if (!error){
            user = db.userLogin(email.getText().toString(), password.getText().toString());

            if(user != null){
                toastMessage("Welcome " + user.getUsername());
            }else {
                toastMessage("Wrong email or password");
                return;
            }


            session.isLoggedIn(true, user.getEmail());
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            intent.putExtra("userEmail", user.getEmail());
            intent.putExtra("userName", user.getUsername());
            startActivity(intent);
        }
    }

    public void Register (View view){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}
