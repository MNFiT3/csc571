package com.mnfit3dev.labassignment1;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;


public class Fragment_HobbiesSkills extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_hobbies_skills, container, false);
        ListView hobbyList = view.findViewById(R.id.hobbyLists);
        ListView skillList = view.findViewById(R.id.skilllist);

        final ArrayList<String> hobby = new ArrayList<>();
        final ArrayList<String> leSkills = new ArrayList<>();

        for(int i = 0; i < 20; i++){
            leSkills.add("List - " + i);
        }

        hobby.add("Programming");
        hobby.add("Gaming");
        hobby.add("Watch Anime");
        hobby.add("Repairing Electronics");
        hobby.add("Try to invent something with electronics");

        ArrayAdapter<String> adapterSkill = new ArrayAdapter<>(view.getContext(), R.layout.support_simple_spinner_dropdown_item, leSkills);
        skillList.setAdapter(adapterSkill);

        ArrayAdapter<String> adapterHobby = new ArrayAdapter<>(view.getContext(), R.layout.support_simple_spinner_dropdown_item, hobby);
        hobbyList.setAdapter(adapterHobby);

        final Button btn = (Button) view.findViewById(R.id.btn0);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://bitbucket.org/MNFiT3/")));
            }
        });

        return view;
    }

}
