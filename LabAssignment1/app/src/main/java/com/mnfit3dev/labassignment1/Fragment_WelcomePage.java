package com.mnfit3dev.labassignment1;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


public class Fragment_WelcomePage extends Fragment {
    WebView webView;
    Session session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        session = new Session(getContext());
        View view = inflater.inflate(R.layout.fragment_welcome_page, container, false);

        TextView txtUsername = view.findViewById(R.id.txtUsername);
        txtUsername.setText(session.userEmail());

        return view;
    }
}
