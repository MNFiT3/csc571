package com.mnfit3dev.labassignment1.model;

public class User {
    String ID, Username, Email, Password;

    public User(){}

    public User(String ID, String username, String email, String password) {
        this.ID = ID;
        Username = username;
        Email = email;
        Password = password;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID='" + ID + '\'' +
                ", Username='" + Username + '\'' +
                ", Email='" + Email + '\'' +
                ", Password='" + Password + '\'' +
                '}';
    }

    //Create a table SQL
    public String TABLE_NAME(){
        return "user";
    }

    public String TABLE_ATT_ID(){ return "id"; }
    public String TABLE_ATT_EMAIL(){ return "email"; }
    public String TABLE_ATT_USERNAME(){ return "username"; }
    public String TABLE_ATT_PASSWORD(){ return "password"; }

    public String CREATE_TABLE(){
        return "CREATE TABLE " + TABLE_NAME() + " ( " +
                TABLE_ATT_ID() + " INTEGER PRIMARY KEY," +
                TABLE_ATT_EMAIL() + " TEXT," +
                TABLE_ATT_USERNAME() + " TEXT," +
                TABLE_ATT_PASSWORD() + " TEXT " +
                ")";
    }
}
