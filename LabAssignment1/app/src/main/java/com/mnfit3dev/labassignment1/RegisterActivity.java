package com.mnfit3dev.labassignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mnfit3dev.labassignment1.model.User;

public class RegisterActivity extends Activity {
    TextView txtEmailRegister, txtRePasswordRegister, txtPasswordRegister, txtUsernameRegister, txtToLogin;
    Button btnRegister;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        db = new DBHelper(getApplicationContext());

        txtUsernameRegister = findViewById(R.id.txtUsernameRegister);
        txtEmailRegister = findViewById(R.id.txtEmailRegister);
        txtPasswordRegister = findViewById(R.id.txtPasswordRegister);
        txtRePasswordRegister = findViewById(R.id.txtRePasswordRegister);
        btnRegister = findViewById(R.id.btnRegister);
        txtToLogin = findViewById(R.id.txtToLogin);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;

                if(txtUsernameRegister.getText().length() <= 3){
                    Toast.makeText(getApplicationContext(), "Username must minimum of 3 char", Toast.LENGTH_SHORT).show();
                    error = true;
                }

                if(txtEmailRegister.getText().length() <= 3){
                    Toast.makeText(getApplicationContext(), "Email cannot be empty", Toast.LENGTH_SHORT).show();
                    error = true;
                }

                if(txtPasswordRegister.getText().toString().length() <= 8){
                    Toast.makeText(getApplicationContext(), "Password must minimum 8 char" ,Toast.LENGTH_SHORT).show();
                    error = true;
                }

                if(!txtPasswordRegister.getText().toString().equals(txtRePasswordRegister.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Password Not Match" ,Toast.LENGTH_SHORT).show();
                    error = true;
                }

                if(!error){
                    if(db.userIsRegistred(txtEmailRegister.getText().toString())){
                        Toast.makeText(getApplicationContext(), "Email is already registered" ,Toast.LENGTH_SHORT).show();
                    }else{
                        try{
                            db.userRegister(new User(null,
                                    txtUsernameRegister.getText().toString(),
                                    txtEmailRegister.getText().toString(),
                                    txtPasswordRegister.getText().toString()
                            ));
                            Toast.makeText(getApplicationContext(), "Success" ,Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }catch (Exception e){
                            Toast.makeText(getApplicationContext(), "Error register user" ,Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
        });

        txtToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });
    }

    public void Register(View view) {




    }
}
