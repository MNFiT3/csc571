package com.mnfit3dev.labassignment2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mnfit3dev.labassignment2.model.User;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sutorage";

    private static final User TABLE_USER = new User();

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_USER.CREATE_TABLE());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER.TABLE_NAME());
        onCreate(db);
    }

    public void userRegister(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_USER.TABLE_ATT_USERNAME(), user.getUsername());
        values.put(TABLE_USER.TABLE_ATT_EMAIL(), user.getEmail());
        values.put(TABLE_USER.TABLE_ATT_PASSWORD(), user.getPassword());

        db.insert(TABLE_USER.TABLE_NAME(), null, values);
    }

    public User userLogin(String email, String password) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "select * from " + TABLE_USER.TABLE_NAME() +
                " where " + TABLE_USER.TABLE_ATT_EMAIL() +
                " = '" + email +
                "' and " + TABLE_USER.TABLE_ATT_PASSWORD() + " = '" + password + "'";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
        }else{
            Log.w("DB", "User not exist");
            return null;
        }

        User usr = new User(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3)
        );

        if(!(usr.getPassword().equals(password))){
            System.out.println("Password mismatch");
            return null;
        }

        return usr;
    }

    public boolean userIsRegistred(String email){
        String query = "select * from " + TABLE_USER.TABLE_NAME() +
                " where " + TABLE_USER.TABLE_ATT_EMAIL() +
                " = '" + email + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        return (cursor.getCount() >= 1)? true : false;
    }

    public int userCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USER.TABLE_NAME();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }
}
